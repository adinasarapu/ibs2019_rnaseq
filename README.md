# IBS 574 - Computational Biology & Bioinformatics, Spring 2019, Emory University

# RNA-seq data analysis: raw reads to differential expression

The following pipeline is intended to help Emory University (IBS-574 class) students in analyzing RNA-sequencing data. A basic knowledge of the usage of the Linux shell is required [^1].

A typical workflow for RNA-seq data analysis consists of the following steps:

 * Connect to Emory's Human Genetics Computing Cluster (HGCC [^2],[^3])
 * Create project directories
 * Get the raw data
 * Run the following scripts:

| Script		| Job type 	|	Description		|
| --------------------- | ------------- | ----------------------------- |
| fastqc_step1.sh	| batch		| Sequence quality control	|
| star_step2.sh		| batch		| Read alignment/mapping	|
| htseq_step3.sh	| batch		| Gene count			|
| diff_exp_step4.R	| interactive	| Differential expression	|

 * Use correct program (**qsub** or **Rscript**) and file path (**relative path** or **absolute path**) to run your script.  
 * To submit a **batch job** to the HGCC queues, use the `qsub` command followed by the name of your batch script file.  
 * When submitting an **interactive job**, use **qlogin** to establish a session on a compute node and load required module (ex. R) and run the script.  

## 1. Connect to the HPC Cluster via SSH  

SSH allows you to connect to the Human Genetics Compute Cluster (HGCC) server securely and perform linux command-line operations.  
`ssh <user_name>@hgcc.genetics.emory.edu`  

Alternatively, open a web browser (Safari or Google Chrome) and enter the following URL into your browser address bar, then press enter or return.  
`https://hgcc.genetics.emory.edu:22443`  

## 2. Create project directory and sub-directories  

From your home directory i.e /home/<user_name>  
```
mkdir -p transcriptomics/{data,out,logs}
```
Note: no spaces between data,out,logs  

## 3. Copy the compressed raw data into the data directory and then uncompress it  

```
cp /scratch/ibs.rnaseq.tar.gz ~/transcriptomics/data/
cd ~/transcriptomics/data
tar -xvzf ibs.rnaseq.tar.gz
```

Make sure all (12) fastq.gz files and a txt file really exist in `~/transcriptomics/data/ibs.rnaseq/` directory  
`ls ~/transcriptomics/data/ibs.rnaseq/`  

Each of the following fastq.gz raw data file contains ~10 million reads (only 20% of the > 50 million reads per sample).  
Even with these small datasets, some of the analysis steps may take hours.

```
Human_R1_SL100145.fastq.gz
Human_R1_SL100146.fastq.gz
Human_R1_SL100147.fastq.gz
Human_R1_SL100149.fastq.gz
Human_R1_SL100150.fastq.gz
Human_R1_SL100151.fastq.gz
Human_R2_SL100145.fastq.gz
Human_R2_SL100146.fastq.gz
Human_R2_SL100147.fastq.gz
Human_R2_SL100149.fastq.gz
Human_R2_SL100150.fastq.gz
Human_R2_SL100151.fastq.gz
meta_data.txt
```  

## 4. Copy RNA-seq analysis warapper scripts  

Copy compressed scripts.tar.gz file into `~/transcriptomics` directory and extract it  

```
cp /scratch/scripts.tar.gz ~/transcriptomics
cd ~/transcriptomics
tar -xvzf scripts.tar.gz
```

Make sure the following four files really exist in `~/transcriptomics/scripts`

`ls ~/transcriptomics/scripts`  

```
fastqc_step1.sh
star_step2.sh
htseq_step3.sh
diff_exp_step4.R
```
Note: Update the following lines from all of the above scripts  
```
#$ -M your_email_id@emory.edu
```

## 5. Quality control of raw reads, FastQC [^4]

Submit your first job from the `~/transcriptomics/logs` sub-directory as  
`qsub ~/transcriptomics/scripts/fastqc_step1.sh`  

If you submit it correctly, you will see a message like the following  
```
hgcc:node00:[logs] % qsub ~/transcriptomics/scripts/fastqc_step1.sh
Your job 302308 ("fastqc") has been submitted
```

To check the status of all current jobs you submitted, use the `qstat` command  
`hgcc:node00:[logs] % qstat`  

| job-ID  | prior   | name   | user         | state | submit/start at     | queue               | slots ja-task-ID | 
| ------- | ------- | ------ | ------------ | ----- | ------------------- | ------------------- | ---------------- |
|  302308 | 1.25489 | fastqc   adinasarapu  | r     | 03/13/2019 11:46:13 | ibs574@node09.local | 4  		   |


Wait! until the above submitted job completes!  

Run **MultiQC**[^5] as an interactive job with a specified  **FastQC** output directory i.e `fastqc`.

```
qlogin
cd ~/transcriptomics/out
module load Anaconda2/4.2.0
multiqc fastqc --outdir fastqc_multiqc
module unload Anaconda2/4.2.0
exit
```

Copy the `fastqc_multiqc` directory into your local computer using FTP client and explore the results.  

## 6. Map/Align raw reads to a reference genome, STAR [^6]

Submit your second job from the `~/transcriptomics/logs` sub-directory as
```
qsub ~/transcriptomics/scripts/star_step2.sh
```
Again, to check the status of all current jobs submitted by you, use the `qstat` command. 
Wait until the submitted job completes! Read mapping creates a `bam` file per sample. Check the STAR output using the following command  

`ls ~/transcriptomics/out/mapping/SL1001*/`

**STAR output per sample**  
```
Aligned.sortedByCoord.out.bam  
Aligned.sortedByCoord.out.bam.bai  
Log.final.out  
Log.out  
Log.progress.out  
SJ.out.tab  
_STARgenome  
_STARtmp
```

**Read mapping stats per sample**  
```
cd ~/transcriptomics/out/mapping/SL100145  
cat Log.final.out  
```
|						 |			|
| ---------------------------------------------: | :-------------------	|
|                                 Started job on |	Mar 14 13:24:14	|
|                             Started mapping on |	Mar 14 13:29:26	|
|                                    Finished on |	Mar 14 13:34:23	|
|       Mapping speed, Million of reads per hour |	138.35		|
|						 |			|
|                          Number of input reads |	11413887	|
|                      Average input read length |	200		|
|                          **UNIQUE READS:**	 |			|
|                   Uniquely mapped reads number |	10464373	|
|                        Uniquely mapped reads % |	91.68%		|
|                          Average mapped length |	197.64		|
|                       Number of splices: Total |	7539318		|
|            Number of splices: Annotated (sjdb) |	7390925		|
|                       Number of splices: GT/AG |	7465614		|
|                       Number of splices: GC/AG |	52387		|
|                       Number of splices: AT/AC |	5997		|
|               Number of splices: Non-canonical |	15320		|
|                      Mismatch rate per base, % |	0.45%		|
|                         Deletion rate per base |	0.01%		|
|                        Deletion average length |	1.55		|
|                        Insertion rate per base |	0.01%		|
|                       Insertion average length |	1.36		|
|                   **MULTI-MAPPING READS:**	 |			|
|        Number of reads mapped to multiple loci |	382233		|
|             % of reads mapped to multiple loci |	3.35%		|
|        Number of reads mapped to too many loci |	5065		|
|             % of reads mapped to too many loci |	0.04%		|
|                        **UNMAPPED READS:**	 |			|
|       % of reads unmapped: too many mismatches |	0.00%		|
|                 % of reads unmapped: too short |	4.90%		|
|                     % of reads unmapped: other |	0.03%		|
|                        **CHIMERIC READS:**	 |			|
|                       Number of chimeric reads |	0		|
|                            % of chimeric reads |	0.00%		|



**Read mapping summary stats using MultiQC**

Then run `multiqc` as an interactive job on the `STAR` output directory.
```
qlogin
cd ~/transcriptomics/out
module load Anaconda2/4.2.0
multiqc mapping --outdir mapping_multiqc
module unload Anaconda2/4.2.0
exit
```

Copy the `mapping_multiqc` directory into your local computer using an `FTP` client and explore the results.


## 7. Quantification of reads against genes, HTSeq [^7]

Submit your third job from the `~/transcriptomics/logs` sub-directory as

```
qsub ~/transcriptomics/scripts/htseq_step3.sh
```
Wait until the submitted job completes! 

Read quantification creates a file including read counts for each sample. Check the output using the following command  

`ls ~/transcriptomics/out/HTSeq/`  

Then combine sample-wise gene counts data into a single table using `Excel` or use the following scripts.    

| Command  | description						|
| -------- | :--------------------------------------------------------- |
| `paste`  | command: merges files by columns				|  
| `cut`    | command: extracts columns from the merged file		|	  
|  |       | pipe to connect Linux functions into one single command	|  
| `cat`    | command sends it's output to stdout (standard output)	|

```
qlogin
cd ~/transcriptomics/out/HTSeq
paste SL100145.counts SL100146.counts SL100147.counts SL100149.counts SL100150.counts SL100151.counts | cut -f1,2,4,6,8,10,12 > CountMerged.txt
echo -e "id\tSL100145\tSL100146\tSL100147\tSL100149\tSL100150\tSL100151" | cat - CountMerged.txt > tmp_count && mv tmp_count CountMerged.txt
exit
```

**HTSeq output for all samples** i.e `CountMerged.txt` file (~26491 genes or features).  

| id			 | SL100145 | SL100146 | SL100147 | SL100149 | SL100150 | SL100151 |
| :---			 | :---	    | :---     | :---     | :---     | :---     | :---     |
| A1BG			 | 38	    | 50       | 43	  | 16	     | 28	| 25       |
| A1BG-AS1		 | 3	    | 5	       | 3	  | 3	     | 3	| 3        |
| A1CF			 | 0	    | 2	       | 3	  | 2	     | 4	| 6        |
| A2M			 | 15	    | 10       | 6	  | 8	     | 19	| 13       |
| A2M-AS1		 | 5	    | 3	       | 26	  | 6	     | 5	| 5        |
| A2ML1			 | 79	    | 85       | 15	  | 43	     | 55	| 68       |
| A2MP1			 | 0	    | 0	       | 1	  | 0	     | 1	| 1        |
| ...			 | ...      | ...      | ...      | ...      | ...      | ...      |
| ZYX			 | 1467	    | 1442     | 1220	  | 1007     | 1454	| 2461     |
| ZZEF1			 | 382	    | 328      | 367	  | 274	     | 364	| 468      |
| ZZZ3			 | 695	    | 623      | 522	  | 447	     | 639	| 795      |
| __no_feature		 | 1492055  | 1463626  | 1873538  | 1362400  | 1435324	| 1921222  |
| __ambiguous		 | 250500   | 242334   | 234837	  | 188096   | 228558   | 275707   |
| __too_low_aQual	 | 0	    | 0	       | 0	  | 0	     | 0	| 0        |
| __not_aligned		 | 0	    | 0	       | 0	  | 0	     | 0	| 0        |
| __alignment_not_unique | 1105216  | 1667808  | 1448423  | 828312   | 1203362	| 1922707  |

## 8. Differential expression analysis, edgeR [^8]

1. Gene filtering  
   Keep genes with counts (> 5) in at least 3 (50%) samples. Filtering eliminates low-count genes that have high dispersions/variances.    
2. Normalization  
   Perform trimmed mean of M-values (TMM) normalization (using edgeR, R package) and get the normalization factors. The main aim in TMM normalization is to account for library size variation between samples of interest. After filtering, it is a good idea to reset the library sizes. The library size and normalization factors are multiplied together to act as the effective library size.
3. Differential Expression analysis of genes between two groups  
   **t-test**: Convert TMM normalized data to Count Per Million(CPM) and then log2(CPM): Counts scaled by total number of reads (normalization for library size) necessary for comparison of expression of the same gene between samples. The t-test assumes normal distribution (within the groups). This assumption is reasonable for log(expression)-values. Then, false discovery rate (FDR), which is the expected fraction of false positives in a list of genes selected following a particular statistical procedure.  
   **edgeR**, in classic approach, tests for differential expression between two groups using a method similar in idea to the Fisher's Exact Test. This test is for a two group comparison, but if you have additional covariates, you'll need to use a generalized linear model (GLM) framework.

To run an R script, use `qlogin` to establish a session on a compute node  

```
qlogin
module load R/3.4.3 
Rscript ~/transcriptomics/script/diff_exp_step4.R
module unload R/3.4.3
exit
```

[^1]: [I](http://adinasarapu.github.io/files/2018_Linux_Shell_I.pdf) & [II](http://adinasarapu.github.io/files/2018_Linux_Shell_II.pdf), Linux shell & shell scripting
[^2]: [HGCC - Job submission](http://wingolab.org/hgcc/), Emory's Human Genetics Compute Cluster.
[^3]: [HGCC - Support center](https://hgcc.genetics.emory.edu), Emory's Human Genetics Compute Cluster.
[^4]: [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/), A quality control tool for high throughput sequence data.
[^5]: [MultiQC](https://multiqc.info), aggregate results from bioinformatics analyses across many samples into a single report. 
[^6]: [STAR](https://github.com/alexdobin/STAR), Spliced Transcripts Alignment to a Reference.  
[^7]: [HTSeq](https://htseq.readthedocs.io), Analysing high-throughput sequencing data with Python.  
[^8]: [edgeR](http://bioconductor.org/packages/release/bioc/html/edgeR.html), Empirical Analysis of Digital Gene Expression Data in R

## Note -   

**~** (tilde) is a Linux "shortcut" to denote a user's home directory.  
`$HOME` is an environment variable that contains the location of your home directory.  
**~** is equal to **/home/<your_user_name>** directory.  
**~/** is equal to **/home/<your_user_name>/** directory.

## References
