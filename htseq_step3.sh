#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D                      #
# Emory University, Dept. of Human Genetics     #
# Emory Integrated Computational Core           #
# ashok.reddy.dinasarapu@emory.edu              #
# Last updated on 03/15/2019                    #
#################################################

# Note that all qsub directives start with '#$' while comment line starts with '#'

# name for the job 
#$ -N htseq

# the cluster resources (the nodes) are grouped into Queues.
#$ -q ibs574

# parallel environment (-pe) requesting 9 slots/cores
#$ -pe smp 5

# current working directory.
#$ -cwd

# merge standard error with standard output.
#$ -j y

# e-mail notification options.
#$ -m abe
#$ -M user_name@emory.edu

PROJ_DIR=$HOME/transcriptomics
OUT_DIR=$PROJ_DIR/out/HTSeq

# Create a sub-directory for output
if [ ! -d $OUT_DIR ]; then
 /bin/mkdir -p $OUT_DIR
fi

# Reference Genome Annotations directory
ANNOTATION_DIR=/sw/hgcc/Data/Illumina/Homo_sapiens/UCSC/hg38/Annotation/Genes
MAP_DIR=$PROJ_DIR/out/mapping

# STAR aligned BAM files  
BAM_FILE_45=$MAP_DIR/SL100145/Aligned.sortedByCoord.out.bam
BAM_FILE_46=$MAP_DIR/SL100146/Aligned.sortedByCoord.out.bam
BAM_FILE_47=$MAP_DIR/SL100147/Aligned.sortedByCoord.out.bam
BAM_FILE_49=$MAP_DIR/SL100149/Aligned.sortedByCoord.out.bam
BAM_FILE_50=$MAP_DIR/SL100150/Aligned.sortedByCoord.out.bam
BAM_FILE_51=$MAP_DIR/SL100151/Aligned.sortedByCoord.out.bam

# Load a bioinformatic package  
module load Anaconda2/4.2.0

# htseq-count from STAR aligned BAM files
htseq-count -f bam -m union -r pos -i gene_id -a 10 -s no $BAM_FILE_45 $ANNOTATION_DIR/genes.gtf > $OUT_DIR/SL100145.counts
htseq-count -f bam -m union -r pos -i gene_id -a 10 -s no $BAM_FILE_46 $ANNOTATION_DIR/genes.gtf > $OUT_DIR/SL100146.counts
htseq-count -f bam -m union -r pos -i gene_id -a 10 -s no $BAM_FILE_47 $ANNOTATION_DIR/genes.gtf > $OUT_DIR/SL100147.counts
htseq-count -f bam -m union -r pos -i gene_id -a 10 -s no $BAM_FILE_49 $ANNOTATION_DIR/genes.gtf > $OUT_DIR/SL100149.counts
htseq-count -f bam -m union -r pos -i gene_id -a 10 -s no $BAM_FILE_50 $ANNOTATION_DIR/genes.gtf > $OUT_DIR/SL100150.counts
htseq-count -f bam -m union -r pos -i gene_id -a 10 -s no $BAM_FILE_51 $ANNOTATION_DIR/genes.gtf > $OUT_DIR/SL100151.counts

# Unload the package
module unload Anaconda2/4.2.0
 
# -m union: union is one of three options for htseq count for binning of
# 	reads to features (genes/exons).  it is the recommended option per
# 	htseq count’s manual.  It is the most stringent option as it will only
# 	count a read if it aligns to only 1 gene.
# -r pos : meaning that the BAM/SAM file is sorted by genomic coordinate/position
# -i gene_name : tells htseq-count to use the gene name in output file
# -a 10 : tells htseq-count to skip reads with alignment score less than 10
# –stranded=no : the RNAseq reads are not strand specific
