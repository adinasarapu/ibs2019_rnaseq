#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D                      #
# Emory University, Dept. of Human Genetics     #
# Emory Integrated Computational Core		#
# ashok.reddy.dinasarapu@emory.edu              #
# Last updated on 03/15/2019                    #
#################################################

## name for the job
#$ -N fastqc

## The cluster resources (the nodes) are grouped into Queues.
#$ -q ibs574

## request parallel environment (-pe) requesting 4 slots/cores.
#$ -pe smp 4

## current working directory.
#$ -cwd

## merge standard error with standard output.
#$ -j y

## email options about the status of your job.
#$ -m abe
#$ -M user_name@emory.edu

PROJ_DIR=$HOME/transcriptomics
SEQ_DATA_DIR=$PROJ_DIR/data/ibs.rnaseq
OUT_DIR=$PROJ_DIR/out

module load FastQC/0.11.4

# QC sub-directory will be created 
if [ ! -d $OUT_DIR/fastqc ]; then
 mkdir -p $OUT_DIR/fastqc
fi

# QC metrics for all of FASTQs
fastqc -t 9 $SEQ_DATA_DIR/*.fastq.gz -o $OUT_DIR/fastqc

module unload FastQC/0.11.4
