#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D                      #
# Emory University, Dept. of Human Genetics     #
# Emory Integrated Computational Core           #
# ashok.reddy.dinasarapu@emory.edu              #
# Last updated on 03/15/2019                    #
#################################################

## name for the job.
#$ -N star

## The cluster resources (the nodes) are grouped into Queues.
#$ -q ibs574

## parallel environment (-pe) requesting 5 slots/cores/threads.
#$ -pe smp 5

## current working directory.
#$ -cwd

## merge standard error with standard output.
#$ -j y

## email options about the status of your job.
#$ -m abe
#$ -M user_name@emory.edu

PROJ_DIR=$HOME/transcriptomics
DATA_DIR=$PROJ_DIR/data/ibs.rnaseq
OUT_DIR=$PROJ_DIR/out

SEQUENCE_DIR=/sw/hgcc/Data/Illumina/Homo_sapiens/UCSC/hg38/Sequence/WholeGenomeFasta
ANNOTATION_DIR=/sw/hgcc/Data/Illumina/Homo_sapiens/UCSC/hg38/Annotation/Genes

module load STAR/2.5.3a
module load samtools/1.9

# make sub-directories for output
if [ ! -d $OUT_DIR/mapping ]; then
  mkdir -p $OUT_DIR/mapping/{index,SL100145,SL100146,SL100147,SL100149,SL100150,SL100151}
fi
 
# build STAR indexing of reference genome, hg38

STAR --runMode genomeGenerate \
 --genomeDir $OUT_DIR/mapping/index \
 --genomeFastaFiles $SEQUENCE_DIR/genome.fa \
 --runThreadN 5

# For sample : SL100145
# Read alignment to refrence genome (or transcriptome)
STAR --runMode alignReads \
 --genomeDir $OUT_DIR/mapping/index/ \
 --readFilesIn $DATA_DIR/Human_R1_SL100145.fastq.gz $DATA_DIR/Human_R2_SL100145.fastq.gz \
 --runThreadN 5 \
 --readFilesCommand zcat \
 --sjdbGTFfile $ANNOTATION_DIR/genes.gtf \
 --outSAMtype BAM SortedByCoordinate \
 --outFileNamePrefix $OUT_DIR/mapping/SL100145/
# create bam index file
samtools index $OUT_DIR/mapping/SL100145/Aligned.sortedByCoord.out.bam

# For sample : SL100146
# Read alignment to refrence genome (or transcriptome)
STAR --runMode alignReads \
 --genomeDir $OUT_DIR/mapping/index/ \
 --readFilesIn $DATA_DIR/Human_R1_SL100146.fastq.gz $DATA_DIR/Human_R2_SL100146.fastq.gz \
 --runThreadN 5 \
 --readFilesCommand zcat \
 --sjdbGTFfile $ANNOTATION_DIR/genes.gtf \
 --outSAMtype BAM SortedByCoordinate \
 --outFileNamePrefix $OUT_DIR/mapping/SL100146/
# create bam index file
samtools index $OUT_DIR/mapping/SL100146/Aligned.sortedByCoord.out.bam

# For sample : SL100147
# Read alignment to refrence genome (or transcriptome)
STAR --runMode alignReads \
 --genomeDir $OUT_DIR/mapping/index/ \
 --readFilesIn $DATA_DIR/Human_R1_SL100147.fastq.gz $DATA_DIR/Human_R2_SL100147.fastq.gz \
 --runThreadN 5 \
 --readFilesCommand zcat \
 --sjdbGTFfile $ANNOTATION_DIR/genes.gtf \
 --outSAMtype BAM SortedByCoordinate \
 --outFileNamePrefix $OUT_DIR/mapping/SL100147/
# create bam index file
samtools index $OUT_DIR/mapping/SL100147/Aligned.sortedByCoord.out.bam

# For sample : SL100149
# Read alignment to refrence genome (or transcriptome)
STAR --runMode alignReads \
 --genomeDir $OUT_DIR/mapping/index/ \
 --readFilesIn $DATA_DIR/Human_R1_SL100149.fastq.gz $DATA_DIR/Human_R2_SL100149.fastq.gz \
 --runThreadN 5 \
 --readFilesCommand zcat \
 --sjdbGTFfile $ANNOTATION_DIR/genes.gtf \
 --outSAMtype BAM SortedByCoordinate \
 --outFileNamePrefix $OUT_DIR/mapping/SL100149/
# create bam index file
samtools index $OUT_DIR/mapping/SL100149/Aligned.sortedByCoord.out.bam

# For sample : SL100150
# Read alignment to refrence genome (or transcriptome)
STAR --runMode alignReads \
 --genomeDir $OUT_DIR/mapping/index/ \
 --readFilesIn $DATA_DIR/Human_R1_SL100150.fastq.gz $DATA_DIR/Human_R2_SL100150.fastq.gz \
 --runThreadN 5 \
 --readFilesCommand zcat \
 --sjdbGTFfile $ANNOTATION_DIR/genes.gtf \
 --outSAMtype BAM SortedByCoordinate \
 --outFileNamePrefix $OUT_DIR/mapping/SL100150/
# create bam index file
samtools index $OUT_DIR/mapping/SL100150/Aligned.sortedByCoord.out.bam

# For sample : SL100151
# Read alignment to refrence genome (or transcriptome)
STAR --runMode alignReads \
 --genomeDir $OUT_DIR/mapping/index/ \
 --readFilesIn $DATA_DIR/Human_R1_SL100151.fastq.gz $DATA_DIR/Human_R2_SL100151.fastq.gz \
 --runThreadN 5 \
 --readFilesCommand zcat \
 --sjdbGTFfile $ANNOTATION_DIR/genes.gtf \
 --outSAMtype BAM SortedByCoordinate \
 --outFileNamePrefix $OUT_DIR/mapping/SL100151/
# create bam index file
samtools index $OUT_DIR/mapping/SL100151/Aligned.sortedByCoord.out.bam

rm -rf $OUT_DIR/mapping/index

module unload samtools/1.9
module unload STAR/2.5.3a
